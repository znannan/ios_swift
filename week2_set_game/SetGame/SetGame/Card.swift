//
//  Card.swift
//  SetGame
//
//  Created by znn_ios on 2019/4/18.
//  Copyright © 2019年 Olivet University. All rights reserved.
//

import Foundation
class Card: Equatable <#super class#> {
    <#code#>let numberOfShape: NumverOfShape
    let shape: shape
    let shading: shading
    let color: color
    var description: String{
        return "\(String(self.numverOfShape.rawValue)),\(self.shape.rawValue),\(self.shading.rawValue),\(self.color.rawValue)"
    }
    
    static func ==(lhv: Card, rhv: Card){
        
    }
    
    enum NumberOfShape:Int, CaseIterable, CustomStringConvertible{
        var description {
            return String (self.rawValue)
        }
        case one = 1
        case two = 2
        case three = 3
        
    }
    
    enum Shape: CaseIterable,CustomStringConvertible{
        var description: String {
            return self.rawValue
        }
        case diamound,squiggle,stadium
        
    }
    enum Shading: CaseIterable,CustomStringConvertible{
    var description: String {
    return self.rawValue
    }
    case solid, striped, open
    }
    enum Color: CaseIterable,CustomStringConvertible{
        var description: String {
            return self.rawValue
        }
        case red,green,purple
    }
}
