//
//  ViewController.swift
//  SetGame
//
//  Created by znn_ios on 2019/4/18.
//  Copyright © 2019年 Olivet University. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var game = Game()

    
    @IBAction func clickCard(_ sender: UIButton){
        
        print("card is clicked")
    }
    
    @IBAction func clickNewGame(_ sender: UIButton){
        game = Game()
        buttonsToCards = [UIButton: Card]()
        cardButtons.shuffle()
        initGame()
        
    }
    func initGame(){
        
        game.initShowingCards(numberOfCards:12)
        for i in 0..< cardButtons.count{
            let button = cardButtons[i]
            if i < game.showingCards.count{
                let card = game.showingCards[i]
                
                buttonsToCards[button] = card
                let attributedContent =
                    getAttributedStringFromCard(card)
                button.setTitle(nil, for: UIControl.State.normal)
                button.setAttributedTitle(attributedContent,for: UIControl.State.normal)
            }else{
                setNoCardButton(button)
            }
            
        }
    }
    @IBOutlet var cardButtons: [UIButton]! {
        didSet{
            cardButtons.shuffle()
            initGame()
            //print(buttonToCards.keys.count)
        }
    }
    
    var buttonsToCards = [UIButton: Card]
    
    override func viewDidLoad(){
        super.viewDidLoad()
        game.initShowingCards(numverOfCards:12)
        for card in game.showingCards{
            print(card)
        }
    }
    
    extension ViewController{
        func getAttributedStringFromCard(_ card: Card)
            ->NSAttributedString{
                
        }
        func getStringFromCard(_ card: Card)->String{
            var shape = ""
            switch card.shape{
            case .diamond:
                return String(repeating: "🔺", count: card.numberOfShape.rawValue)
                
            }
            case .squiggle:
                return String(repeating: "", count:
                card.numverOfShape.rawValue)
            case .stadium:
                return String(repeating: "", count:
            card.numverOfShape.rawValue)
            
        }
        func getColorFromCard(_ card: Card){
            switch card.color{
            case .green:
                return UIColor.green
            case .purple:
                return UIColor.purple
            case .red:
                return UIColor.red
            }
        }
        func setNoCardButton(_ button: UIButton){
            button.setTitle(nil, for: UIC)
        }
    }
    

}

