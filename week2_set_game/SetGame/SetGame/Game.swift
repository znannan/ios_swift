//
//  Game.swift
//  SetGame
//
//  Created by znn_ios on 2019/4/18.
//  Copyright © 2019年 Olivet University. All rights reserved.
//

import Foundation

struct Game {
    static let bonus = 5
    static let penalty = 3
    <#fields#>var cards = [Card]()
    var showingCards = [Card]()
    var selectedCards = [Card]()
    var score = 0
    init(){
        initCards()
        shuffleCards()
    }
    private mutating func initCards(){
        for numberOfShape in Card.NumverOfShape.allCases{
            for shape in Card.Shape.allCases{
                for shading in Card.Shading.allCases{
                    for color in Card.Color.allCases{
                        cards.append(Card(numberOfShape:numverOfShape, shape: shape, shading:shading, color:color))
                    }
                }
            }
        }
    }
    func initShowingCards(numverOfCards: Int){
        assert(numberOfCards <= cards.count,
               "Game.initShowingCards(\(numberOfCards)), not enought cards to show")
        for i in 0..<numberOfCards{
            showingCards.append(cards.remove(at: i))
            
        }
        
    }
    private mutating func shuffleCards(){
        cards.shuffle()
    }
    func selectCards(card: Card){
        if selectedCards.count <= 3{
            if selectedCards.contains(card){
                    selectedCards.append(card)
            }
        }else{
            if isSelectedCardsSet(){
                score += Game.bone
            }else{
                score -= Game.penalty
            }
        }
    }
}
