//
//  Card.swift
//  Concentration
//
//  Created by znn_ios on 2019/4/11.
//  Copyright © 2019年 Olivet University. All rights reserved.
//

import Foundation

struct Card{
    var isFaceUp = false
    var isMatched = false
    var identifier: Int
    static var identityFactory = 0
    static func getUniqueIdentifier() -> Int{
        identityFactory += 1
        return identityFactory
        
    }
    init(){
        self.identifier = Card.getUniqueIdentifier()
    }
}
